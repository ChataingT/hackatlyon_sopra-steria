export const pageReducer= (address={}, action) => {
    switch (action.type) {
        case 'PAGE':
                return action.page;
        case 'LOAD_ADDRESS':
            return 'map';
        default:
            return address;
        }
    }
export default pageReducer;
