export const mapReducer= (address={}, action) => {
    switch (action.type) {
        case 'LOAD_ADDRESS':
                return action.address;
        default:
            return address;
        }
    }
export default mapReducer;
