import { combineReducers } from 'redux';
import mapReducer from "./mapReducer";
import pageReducer from "./pageReducer"
/*
reducer that can contains set of reducer, usefull when several reducers are used at a timeS
*/
export const globalReducer = combineReducers({
    mapReducer:mapReducer,
    pageReducer:pageReducer
});
export default globalReducer;