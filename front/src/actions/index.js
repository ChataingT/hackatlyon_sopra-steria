
//List of action use to interact with the store via the reducers

export const loadAdress=(address)=>{
    return {type:'LOAD_ADDRESS',address:address};
}

export const root=(page)=>{
    return {type:'PAGE',page:page};
}
