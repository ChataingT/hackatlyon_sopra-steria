import React, { Component } from 'react';
import './lib/css/bootstrap.min.css';
import './lib/Semantic-UI-CSS-master/semantic.min.css'

//import needed to use redux with react.js
import { createStore } from 'redux';
import globalReducer from './reducers'
import { Provider } from 'react-redux';
import { root } from './actions';


//import action to load the first time the list of values

//import component
import pages from "./component/pages";
import color from "./component/color";
import Header from "./component/header/header";
import Footer from "./component/footer/footer";
import Map from "./component/map/map";
import ListEnergie from "./component/energie/listEnergie";
import * as jsonSource from "./component/energie/energies.json";

import EnergieModel from "./component/energie/energieModel";
import listEnergie from './component/header/listEnergie';

//create a store and associate reducers to it

class App extends Component {
  constructor(props){
    super(props);
    this.store = createStore(globalReducer);

    let listNrj = jsonSource.default;
    this.state={
      address:"",
      currentPage:pages.INDEX,
      listEnergie:listNrj.listEnergie,
      selectedEnergie:listNrj.listEnergie[0],
      map:{
        center:[],
        popup:{status:true,
                  coord:[],
                  img:""}}
    }

    //call the action loadItems to load inital items
    /*let list_items=['A','B','C','D','E','F'];
    store.dispatch(loadItems(list_items));
    */


   this.store.dispatch(root(pages.INDEX));
   this.store.subscribe(() => this.rooting()) // probleme de co,texte
  };

  rooting(){
  /**
     * Function for the click event on the button search. 
     * Recuperate the data to load the map
     */
    let currentState = this.store.getState();

    let dataMap=currentState.mapReducer
    let root=currentState.pageReducer
    let map=this.getData(dataMap)
    this.setState({
      address:dataMap,
      currentPage:root,
      map:map,

    });
  }

  getData(data){
    /**
     * With the geo coordinates, load data for the map
     */
    let map={
      center:[4.967760,45.796113],
      popup:{status:true,
                coordinates:[ 4.967760, 45.796113,], 
                img:""}
      };

    return map;
    };
    

  render() {
    let display=<div></div>; // var to return 

    switch(this.state.currentPage){
      case pages.INDEX: // page with the map
        display=
            <div className="ui center aligned middle aligned vertical grid" 
                style={{width:'100%',"boxSizing":'content-box',
                                              "paddingLeft":"0px",
                                              "paddingRight":"0px"}}
                >
                <div className="ui two width row" style={{padding:"0px"}}>
                <Header 
                currentPage={this.state.currentPage}
                address={this.state.address}
                />
                </div>
                <div className="ui one width row">
                <div><Footer/></div>
                </div>
            </div>
        break;
      case pages.MAP:
          display=<div className="ui center aligned middle aligned grid" style={{margin:'0px', 
                                                                        width:'100%',
                                                                        "boxSizing":'content-box',
                                                                        "paddingLeft":"0px",
                                                                        "paddingRight":"0px"}}>
            <Header currentPage={this.state.currentPage}  address={this.state.address}/>
            <div style={{margin:'0px', width:'100%'}} >
              <Map
                center={this.state.map.center}
                listEnergie={this.state.listEnergie}
                popup={this.state.map.popup}/>
            </div>
            <div><Footer/></div>
            </div>;
        break;
      case pages.DETAILS:
        display=<div className="ui container center aligned middle aligned" style={{margin:'0px', 
                        width:'100%', 
                        height:'100%',
                        backgroundColor:"white",
                        "paddingLeft":"0px",
                        "paddingEight":"0px"
                        }}>
                <div style={{marginLeft:"1em", boxSizing:"border-box", height:'100%', backgroundColor:"white"}}>
                <div className="ui three column grid">
                  <div className="middle align column">
                  <img className="ui large column image" src=".\logo3.png" style={{}}/>
                  </div>
                  <div className="column">
                  <h1 className="ui huge header" style={{marginTop:"2em"}}>Potentiel énergétique</h1>
                  </div>
                  </div> 
                  <div className="row">              
                    <div className="col-md-3 col-lg-3" >
                          <ListEnergie listEnergie={this.state.listEnergie} />
                    </div>
                    <div className="col-md-8 col-lg-6" >
                          <EnergieModel
                            display={"all"}/>
                    </div>
                  </div>
                  <div><Footer/></div>
                </div>
                </div>;
              return(<Provider store={this.store} >
                        {display}
                    </Provider>
                )
        break;
      default:
        display=<div>Error 404</div>;
        break;
    }
    return (
      <Provider store={this.store} >
        <div className="ui container center aligned middle aligned" style={{margin:'0px', 
                                              width:'100%', 
                                              height:'100%',
                                              background: 'linear-gradient(to bottom, #00EC00, #237100)',
                                              "paddingLeft":"0px",
                                              "paddingEight":"0px"
                                              }}>
                                                
          {display}

        </div>
      </Provider>
    );
  }
}

export default App;
