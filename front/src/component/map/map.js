import React, { Component } from 'react';
import ReactMapboxGl, { Layer, Feature, Popup } from 'react-mapbox-gl';
import { connect } from 'react-redux';
import { root } from '../../actions';

import ListEnergie from "../energie/listEnergie";


const MapTag = ReactMapboxGl({
    accessToken:
      'pk.eyJ1IjoiY2hhdGFpbmd0IiwiYSI6ImNrMW52d282djBldHEzbmw4djVxZXBtOGMifQ.W7hQk5dGOcx7qrqkYFmXcQ'
  });

class Map extends Component {
/**
 * Container of the Mapbox component
 */  

    constructor(props){
        super(props);
        this.state={
            center:this.props.center,
            zoom:[14],
            popup:this.props.popup
        }

        this.goToMoreInfo=this.goToMoreInfo.bind(this);
    };

    goToMoreInfo(event){
        this.props.dispatch(root("details"));
    }
    render() {
        let display;
        if (true){
            display=<Popup
                            coordinates={this.props.popup.coordinates}
                            offset={{
                                'bottom-left': [12, -38],  'bottom': [0, -38], 'bottom-right': [-12, -38]
                            }}
                            anchor={"left"}>
                        
                        <h1 className="ui green header" style={{marginBottom:"0px"}}>Energie poteniel</h1>
                        <div style={{marginLeft:"2em", boxSizing:"border-box"}}>
                            <div className="row">              
                                        <ListEnergie listEnergie={this.props.listEnergie} />
                            </div>
                        </div>
                        <div className="ui animated centered align middle aligned button" style={{marginBottom:"4px", marginTop:"7px", marginLeft:"1.6em"}} onClick={((ev)=>{this.goToMoreInfo(ev)})}>
                            <div className="visible content">Plus d'information</div>
                            <div className="hidden content" >
                                <i className="right arrow icon"></i>
                            </div>
                        </div>
                    </Popup>;
        }
      return (
        <MapTag
            style="mapbox://styles/mapbox/streets-v9"
            containerStyle={{
                height: '90vh',
                width: '100vw',
                }}
            center={this.state.center}
            zoom={this.state.zoom}
        >
            {display}
      </MapTag>
      )
    };
  }
export default connect() (Map);