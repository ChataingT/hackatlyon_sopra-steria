import React, { Component } from 'react';

class Footer extends Component {
/**
 * Footer shown in all page with two link for about-us and contact-us
 */
/*
            <div className="ui center aligned two column grid">
                <div className="column">
                    <a href="">A propos de nous</a>
                </div>
                <div className="column">
                    <a href="">Nous contacter</a>
                </div>
            </div>
            */
    render() {    
        return (
            
           <div style={{position:"bottom"}}>
                <a className="ui teal button active" role="button" href="" style={{margin:"2em"}}>
                <i className="address card outline icon">
                </i>A propos de nous</a>
                <a target="_blank" className="ui secondary button" role="button" href="" style={{margin:"2em"}}>
                <i aria-hidden="true" className="envelope outline icon">
                </i>Nous contacter</a>
            </div>
        );
      }
    }
    
export default Footer;