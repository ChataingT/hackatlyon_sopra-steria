import React, { Component } from 'react';
import ReactMapboxGl, { Layer, Feature, Popup } from 'react-mapbox-gl';
import { connect } from 'react-redux';
import { root } from '../../actions';


class EnergieModel extends Component {
/**
 * Container of the Mapbox component
 */  

    constructor(props){
        super(props);
        this.state={
            id:this.props.id,
            name:this.props.name,
            icon:this.props.icon,
            mark:this.props.mark,
            display:this.props.display
        }

    };
    getIcon(){
        if (this.state.icon[0]=="."){
            return <img className="ui mini image" src={this.state.icon} style={{width:"50px", margingRight:"4px"}}/>

        }
        else{
            return <i className={"ui big "+ this.state.icon +" icon"} style={{marginRight:"1em"}}></i>
         }
    }

    render(){
        let color;
        let color_tag;
        if(parseInt(this.state.mark) >= 8){
            color="#65fe58";
            color_tag="green";
        }
        else if(parseInt(this.state.mark) >=4 ){
            color="#fea858";
            color_tag="orange";
        }
        else{
            color="#fe795d";
            color_tag="red";
        }
        if (this.state.display == "short"){
            let display=//<div className="ui centered align middle align grid" style={{margin:"0px"}}>
            <div className="panel panel-default" style={{backgroundColor:color, "borderRadius":"20px 10px 5px 15px", marginLeft:"auto", marginRight:'auto'}}>
                <div className="panel-heading">
                    <h3 className="panel-title" style={{textAlign:"center"}}>{this.props.name}</h3>
                </div>
                <div className="panel-body">
                    <div className="ui two centered align middle align column grid" style={{marginBottom:"4px", marginTop:"0px", marginRight:"0px", marginLeft:"0px"}}>
                    {this.getIcon(color_tag)}
                    <a className={"ui " + color_tag + " tag right align label"} style={{pointerEvents:"none", marginLeft:'2em', height:'26px'}}>{this.state.mark + "/10"}</a>
                    
                    </div>
                </div>
            </div>;
            return(display)
        }        
        else if (this.state.display == "all"){
            let display= <div className="ui centered align middle align grid" style={{height:"80%", marginTop:"3.5em"}}>
                                <img className="ui column image" src=".\detail1.png" style={{width:"45%"}}/>
                                <img className="ui  column image" src=".\detail2.png" style={{width:"45%"}}/>
                        </div>;
            return display

        }
        return <div>error</div>;
        
    }
}
export default EnergieModel;