import React, { Component } from 'react';
import EnergieModel from './energieModel';

class listEnergie extends Component {
    //class constructor whith given properties
    constructor(props) {
        super(props);        
        this.getAllEnergieRender=this.getAllEnergieRender.bind(this);
    }
  
 getAllEnergieRender(){
     let array_render=[];
     
     for(var i=0;i<this.props.listEnergie.length;i++){
         
         array_render.push(
             <div style={{margin:"0px"}}>
                 <hr/>
                <EnergieModel
                    key={i}
                    name={this.props.listEnergie[i].name}
                    icon={this.props.listEnergie[i].icon}
                    mark={this.props.listEnergie[i].mark}
                    id={this.props.listEnergie[i].id}
                    display={"short"}
                />
                
             </div>
             );
     }
     return array_render;
 }
    
  //render function use to update the virtual dom
  render() {
      const display_list= this.getAllEnergieRender();
    return (
            <div style={{margin:"0px", width:"90%"}}>
               {display_list}
            </div>
    );
  }
}

//export the current classes in order to be used outside
export default listEnergie;
