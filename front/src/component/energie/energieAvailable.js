/**
 * List of energie available for traitement
 */

export const energieAvailable=["Solaire", "Eolienne", "Hydrolique", "Géothermique", "Organique", "Pluvieux"];

export default energieAvailable;