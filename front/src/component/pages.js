/**
 * page available on the app
 */
export const pages={
    INDEX:"index",
    MAP:"map",
    DETAILS:"details",
    promesse:"Profitez du nouvel outil DYNAMAPS, une carte interactive qui vous permet de mesurer le potentiel énergétique de l'environnement qui entour votre entreprise. Entrez votre adresse dés maintenant et estimé les avantages des différentes innovations du domaine de l'énergie."
  }
export default pages;
