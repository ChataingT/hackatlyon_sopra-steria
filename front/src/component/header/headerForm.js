import React, { Component } from 'react';
import { connect } from 'react-redux';
import { loadAdress } from '../../actions';


class HeaderForm extends Component {
/**
 * Form for the input of address or geographical coordinate
 */
    constructor(props){
        super(props);
        this.state={
            address:this.props.address
        }

    this.validateForm=this.validateForm.bind(this);
    this.onChangeForm=this.onChangeForm.bind(this);
    };

 validateForm(data){
     this.props.dispatch(loadAdress(this.state.address));
     console.log(this.state.address);
     
 };

 onChangeForm(event){
     //console.log(event.currentTarget.value);
     this.setState({
         address:event.currentTarget.value
     });

 };
  render() {    
      return (
          
              <div className="ui left icon action input" style={{height:"25px"}}>
                  <i className="search icon"></i>
                  <input type="text" id="address" onChange={(ev)=>{this.onChangeForm(ev)}}  value={this.state.address} 
                    placeholder="42 rue du sens, City"/>
                  <div className="ui green submit button" style={{paddingTop:"0px", paddingBottom:"0px"}} onClick={(ev)=>{this.validateForm(ev)}}>Search</div>
              </div>
      );
    }
  }
    
export default connect() (HeaderForm);