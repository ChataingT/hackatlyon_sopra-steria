import React, { Component } from 'react';

import HeaderForm from "./headerForm";
import { pages } from "../pages"; //list of all pages
import ListEnergie from "./listEnergie";


class Header extends Component {
/**
 * 
 * @param {*} props -> current page loaded
 */
  constructor(props){
    super(props);
  
    this.state={
        currentPage:this.props.currentPage,
        //address:this.props.address
    };
  }

  render() {
      let display; // var to return 
      switch(this.state.currentPage){
        case pages.INDEX:
            display=
                      <div className="ui centered aligned middle aligned vertical grid" style={{
                        "width":"550px", 
                        "height":"550px",
                        marginTop:"6.7em",
                        "backgroundColor":'white',
                        "marginBottom":"0px",
                        "paddingLeft":"0px",
                        "paddingRight":"0px",
                        "borderRadius":"700px 700px 700px 700px"
                      }}>
                          <div className="ui centered aligned middle aligned vertical grid" style={{marginTop:'3em',
                                                                                                    marginBottom:'2em'
                                                                                                    }}>
                              <img className="ui medium image" src=".\logo_cpt.png" style={{width:"400px", height:"120px", marginTop:"1em"}}/>
                              <h5 className="ui header" style={{marginTop:'0em',
                                                                    }}>{pages.promesse}</h5>
                                <HeaderForm
                                address={this.props.address}/>
                          </div>
                    </div>
          break;
          case pages.MAP:
            display=
                      <div className="ui centered aligned middle aligned vertical grid" style={{"width":"800px", "height":"700",
                      "backgroundColor":'white',
                      "marginBottom":"0px",
                      "paddingLeft":"0px",
                      "paddingRight":"0px",
                      "borderRadius":"700px 700px 0px 0px"
                      }}>
                          <div className="ui centered aligned middle aligned grid" style={{marginTop:'3em'}}>
                          <img className="ui medium image" src=".\logo_cpt.png" style={{width:"70%"}}/>

                            <div className="ui center aligned basic segment">
                                <h1 className="ui green bold header">Adresse ou coordonnées géographiques</h1>
                                  <HeaderForm
                                  address={this.props.address}
                                  />
                            </div>
                          </div>
                          <div className="row"><ListEnergie/></div>    
                    </div>
          break;
        case pages.DETAILS:
          break;
        default:
          display=<div>No header</div>;
          break;
      }
      return (
            display
      );
    }
  }
    
export default Header;