import React, { Component } from 'react';

import energieAvailable from "../energie/energieAvailable";

class listEnergie extends Component {
/**
 *  Display in checkbox all energie available for traitement
 */

    getAllEnergieAvailable() {
        /**
         * Get the list of JSX tag for the energie available 
         */
        let display=[];  
        energieAvailable.forEach(nrj => {
            display.push(
                <div>
                    <div className="ui checkbox">
                        <input type="checkbox" name="example"/>
                        <label className="ui green header" style={{"paddingLeft":"1.2em", "paddingBottom":"1em"}}>{nrj}</label>
                    </div>
                </div>
                );
            
        });
        return display;
    }

    render() {  
        
        let display=this.getAllEnergieAvailable();
        return (
            <div className="ui centered horizontale grid">{display}</div>
            
        );
    }
}
export default listEnergie;