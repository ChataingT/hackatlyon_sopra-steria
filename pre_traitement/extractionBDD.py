import http.client
fichierlistestations = open("listStation.txt", "r", encoding='utf-8')
liste_stations = []
for line in fichierlistestations.readlines():
    sous_liste = line.split("    ")
    liste_stations.append(sous_liste[0])
    
my_dict = {}
for i in liste_stations[:3]:

    conn = http.client.HTTPSConnection("donneespubliques.meteofrance.fr")
    headers = {
        'cache-control': "no-cache",
        'postman-token': "39265255-f0b6-2b3d-5a22-3c14dc5f1325"
        }

    requestString = "/FichesClim/FICHECLIM_" + i + ".data"
    conn.request("GET", requestString, headers=headers)

    res = conn.getresponse()
    data = res.read()
    fichierTampon = open("fichierTampon.txt", "wb")
    fichierTampon.write(data)
    fichierTampon.close()

    #print(data.decode("utf-8"))

    #with open("../input/datatest/FICHECLIM_06029001.data", "r") as data:
    #    with open("fichierTampon.txt", "w") as fichierTampon:
    #        for line in data.readlines(): 
    #            fichierTampon.write(line)
    #        fichierTampon.close()
    #    data.close()


    dict_gen = []
    #with open("../input/datatest/FICHECLIM_06029001.data", "r") as data:
    #    with open("fichierTampon.txt", "w") as fichierTampon:
    #        for line in data.readlines(): 
    #            fichierTampon.write(line)
    #        fichierTampon.close()
    #    data.close()
    list_tampon = []
    #with open("../input/datatest3/FICHECLIM_02320001.data", "r") as data:
    with open("fichierTampon.txt", "r", encoding="utf-8") as data:
        for line in data.readlines(): 
            list_tampon.append(line)    
        data.close()
    #print(list_tampon)
    list_csv = []
    my_dict = {}
    my_dict['ensoleillement'] = ""
    my_dict['vent'] = ""
    my_dict['coord'] = ""
    num_line = 0 # itérateur de ligne
    for case in list_tampon: # Lit chaque ligne du file
        if case == "\n":
            del list_tampon[list_tampon.index(case)]
            
        if case == "Nombre moyen de jours avec fraction d'insolation;\n":
            if list_tampon[list_tampon.index(case)-3] == "Données non disponibles;\n" : 
                None
                
            else :
                index = list_tampon.index(case)
                ensoleillement_annuel = list_tampon[index-3]
                ensoleillement_annuel = ensoleillement_annuel.split(";")
                ensoleillement_annuel = ensoleillement_annuel[-2].split(" ")
               
                my_dict['ensoleillement'] = ensoleillement_annuel[-1]
        
        if case == "Vitesse du vent moyenné sur 10 mn (Moyenne en m/s);\n":
            
            if list_tampon[list_tampon.index(case)+1] == "Données non disponibles;\n" : 
                None
            else :
                index = list_tampon.index(case)
                vent_annuel = list_tampon[list_tampon.index(case)+1]
                vent_annuel = vent_annuel[1:-2]
                vent_annuel = vent_annuel.split(";")
                list_vent = []
                for value in vent_annuel :
                    if vent_annuel.index(value) % 2 == 0:
                        list_vent.append(value)
                        
                moyenne = 0
                
                for element in list_vent[1:]:
                    moyenne = moyenne + float(element[-3:])
                moyenne = moyenne / (len(vent_annuel) -2)
                my_dict['vent'] = moyenne
        
                coordo = list_tampon[3].split(",")   
                coordo = coordo[-2:]
                my_dict['coord'] = coordo
                dict_gen.append(my_dict) 

    print(dict_gen)
                
    
        

                
            