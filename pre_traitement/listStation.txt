01089001    Amberieu
02320001        St Quentin
02321002        Fontaine-les-vv
02594001        Passy En Valois
03060001        Vichy-charmeil
03155003        Lurcy-levis Sa
03180001        Montbeugny
03200001        Neuvy
03248001        St-nicolas
04049001        St Auban
05023001        Briancon
05046001        Embrun
05070003        Laragne Monteglin
06029001        Cannes
06088001        Nice
07068001        Colombier Jeune Rad
07131001        Lanas Syn
08105005        Charleville-mez
09289001        St Girons
10030001        Troyes-barberey
10130001        Dosnon
10228002        Mathaux-etape
11069001        Carcassonne
11170001        Gruissan-inra
12145001        Millau
12254001        Rodez-aveyron
13001006        Aix-les Milles
13001009        Aix En Provence
13004003        Arles
13047001        Istres
13054001        Marignane
13103001        Salon De Provence
14137001        Caen-carpiquet
14624001        L Oudon Lieury_sapc
15014004        Aurillac
15053001        Coltines
15114002        Marcenat
15120005        Mauriac
15122002        Maurs
16089001        Cognac
16113001        La Couronne
16225001        Montemboeuf
16360001        Salles De Barb
16390001        Tusson
16397001        Verdille
17300001        La Rochelle
17415003        Saintes
18033001        Bourges
18092001        Avord
18241002        Sancerre
19010001        Argentat
19031008        Brive
19146001        Naves
19276006        Uzerche
20004002        Ajaccio
20041001        Cap Pertusato
20050001        Calvi
20092001        Conca
20096005        Corte_fdf
20114002        Figari
20148001        Bastia
20223001        Pietralba
20232002        Pila-canale
20258001        Renno
20268001        Sampolo
20272004        Sartene
20342001        Solenzara
21154001        Chatillon/seine
21473001        Dijon-longvic
21567001        Arnay_sapc
22092001        Kerpert
22113006        Lannion_aero
22135001        Louargat
22219003        Plouguenast
22247002        Pommerit-jaudy
22261002        Quintenic
22266001        Rostrenen
22372001        St Brieuc
23030004        Bourganeuf
23176001        La Souterraine
24037005        Bergerac
25056001        Besancon
25462001        Pontarlier
26050001        Besignan
26113003        Die
26115001        Divajeu
26116002        Donzere
26124001        Etoile
26179001        Mercurol
26198001        Montelimar
26313001        St-marcel-les-v-inra
27347001        Evreux-huest
28070001        Chartres
28198001        Chateaudun
28215001        Louville
28239002        Marville
28252002        Miermaigne
28305001        Pre-st-evroult
28322003        Rueil
28406001        Viabon
29021001        Brignogan
29075001        Brest-guipavas
29120001        Lanveoc
29155005        Ouessant-stiff
29163003        Pleyber-christ Sa
29216001        Quimper
29264001        Landivisiau
29276001        Sibiril S A
30081002        Chusclan
30189001        Nimes-courbessac
30258001        Nimes-garons
30339001        Mont Aigoual
31069001        Toulouse-blagnac
31157001        Toulouse-francazal
32013005        Auch
33281001        Bordeaux-merignac
33529001        Cazaux
34154001        Montpellier-aeroport
34301002        Sete
35228001        Dinard
35281001        Rennes-st Jacques
36063001        Chateauroux Deols
37179001        Tours
38384001        Grenoble-st Geoirs
39013004        Arbois_sapc
39097003        Champagnole
39362001        Lons Le Saunier
39413001        La Pesse
39526003        Tavaux Sa
40088001        Dax
40192001        Mont-de-marsan
41097001        Romorantin
41281001        Blois
42005001        St Etienne-boutheon
42095001        Firminy
42170001        Perreux
42184002        Riorges
43062001        Le Puy-loudes
43096001        Fontannes
43111002        Landos-charbon
43234005        Saugues-sa
44020001        Nantes-bouguenais
44103001        St Nazaire-montoir
45055001        Orleans
46127001        Gourdon
47091001        Agen-la Garenne
47252002        Sainte-livrade-sur-lot
48004001        Altier
49020001        Beaucouze
49188001        Marce
51029003        Avize
51183001        Reims-courcy
52269001        Langres
52448001        St-dizier
53094001        Laval-entrammes
53096004        Ernee
54405001        Nancy-ochey
54463001        Toul - Rosieres
54526001        Nancy-essey
56004001        Arzal
56009001        Belle Ile-le Talut
56017003        Bignan
56165003        Ploermel
56185001        Lorient-lann Bihoue
56240003        Sarzeau Sa
56243001        Vannes-sene
57039001        Metz-frescaty
58160001        Nevers-marzy
59343001        Lille-lesquin
60639001        Beauvais-tille
61001001        Alencon
62160001        Boulogne-sem
62298001        Cambrai-epinoy
62548002        Calais-marck
62826001        Le-touquet
63003004        Ambert
63098001        Chastreix
63113001        Clermont-fd
63178001        Issoire
63345002        St-genes-chplle-inra
63353003        St-germain-l He
64010002        Aicirits
64024001        Biarritz-pays-basque
64189001        Socoa
64549001        Pau-uzein
65125001        Campistrous
65344001        Tarbes-lourdes-pyrenees
66002001        Alenya-inra
66136001        Perpignan
66181001        Ste Leocadie
66212001        Torreilles
66233001        Vives
67122001        Wangenbourg_sapc
67124001        Strasbourg-entzheim
68066001        Colmar-inra
68102001        Geishouse_sapc
68205001        Colmar-meyenheim
68224006        Mulhouse
68287003        Rouffach - Sa
68297001        Bale-mulhouse
68338001        Trois-epis_sapc
69029001        Lyon-bron
69174002        Les Sauvages
69299001        Lyon-st Exupery
70132001        Chargey-les-gra
70261001        Frotey_sapc
70473001        Luxeuil
70561002        Villersexel Sa
71014004        Autun
71081001        Chalon-champfo
71105001        Macon
71289001        Matour_sapc
71320001        Mt-st-vincent
71491001        St Yan
72181001        Le Mans
73054001        Bourg St Maurice
73329001        Chambery-aix
74182001        Meythet
75114001        Paris-montsouris
76116001        Rouen-boos
77054001        La Brosse-mx
77113002        Chevru
77211001        Nangis
77306001        Melun
77333003        Nemours
77468001        Torcy
78354001        Magnanville
78465001        Orgerus
78620001        Toussus Le Noble
78621001        Trappes
78640001        Villacoublay
79191005        Niort
80001001        Abbeville
80379002        Amiens-glisy
80682001        Rouvroy-en-santerre
80815002        Vron
81081002        Dourgne
81140002        Lavaur
81284001        Albi
82121002        Montauban
83019002        Bormes Les Mimosas
83031001        Le Luc
83061001        Frejus
83069001        Hyeres
83069016        Hyeres-plage
83137001        Toulon
84025001        Cabrieres D'avignon
84031001        Carpentras
84087001        Orange
85113001        L Ile D Yeu
85191003        La Roche Sur Yon
86027001        Poitiers-biard
87064005        Eymoutiers
87085006        Limoges-bellegarde
87089003        Magnac-laval
87154003        St Junien
87187003        St Yrieix La Pe
88136001        Epinal
89131001        Cruzy_sapc
89206001        Joigny
89346001        Auxerre
90010001        Belfort
91027002        Orly
91103001        Bretigny_sapc
95088001        Le Bourget
95527001        Roissy
97101015        Le Raizet Aero
97209004        Fort-de-france Desaix
97213004        Lamentin-aero
97304003        Kourou Csg
97304005        Kourou Plage
97307001        Cayenne-matoury
97404540        Pont-mathurin
97405480        Petite-ile - Cirad
97407520        Le Port
97410238        Saint-benoit
97411111        Saint-denis College
97412384        St-joseph - Cirad
97413520        Colimacons
97413524        Mascarin
97414431        Le Gol Les Hauts - Cirad
97415566        Piton-maido
97415590        Pointe Des Trois-bassins
97416410        Ravine Des Cabris - Cirad
97416415        Pierrefonds - Cirad
97416463        Pierrefonds-aeroport
97416465        Ligne-paradis - Cirad
97418110        Gillot-aeroport
97422440        Plaine Des Cafres
97424410        Cilaos
97502001        St-pierre
98404001        Kerguelen
98508001        Pamandzi
98611001        Maopoopo
98613001        Hihifo
98715002        Faaa
98723001        Hiva-oa
98741001        Rapa
98753001        Mataura 1
98812001        Koumac
98814001        Ouanaham
98818001        Noumea
98818002        Magenta
98821001        La Tontouta
98822001        Poindimie