import http.client
fichierlistestations = open("listStation.txt", "r", encoding='utf-8')
liste_stations = []
for line in fichierlistestations.readlines():
    sous_liste = line.split("    ")
    liste_stations.append(sous_liste[0])
    
my_dict = {}
for i in liste_stations:

    conn = http.client.HTTPSConnection("donneespubliques.meteofrance.fr")
    headers = {
        'cache-control': "no-cache",
        'postman-token': "39265255-f0b6-2b3d-5a22-3c14dc5f1325"
        }

    requestString = "/FichesClim/FICHECLIM_" + i + ".data"
    conn.request("GET", requestString, headers=headers)

    res = conn.getresponse()
    data = res.read()
    fichierTampon = open("fichierTampon"+str(i)+".txt", "wb")
    fichierTampon.write(data)
    fichierTampon.close()